Gradle Java Application that uses supplied input data to generate an away day Activity Schedule

-I assumed that the input data is supplied as form a path to a local file. 
-This could have been supplied in a config file or as an argument in the command line. I chose the later option as the application will be buid and run with CLI

As this is a Gradle Java Appliation gradle using the gradlle buiild automation system we can use it to perform various tasks, including runnign, 
building and ruuning tests of the project quite quickly.

Prerequisite
In order to run the application properly please install Gradle 4.9 or later (I am using 5.2.1)
from command line navigate to the root directory of the project and run the below command:
$ ./gradlew wrapper --gradle-version 5.2.1 
 
-To run
from command line navigate to the root directory of the project and run the below command:
./gradlew runApp --args='/Documents/activitiesdata.txt'
runApp is the name of out build.gradle task that execute the java application.
'/Documents/activitiesdata.txt' is the path to where the input data text file is located passed to the applicaion as an input argument.

-To build
from command line navigate to the root directory of the project and run the below command:
./gradlew build

-To test
from command line navigate to the root directory of the project and run the below command:
./gradlew test 
Graphical JUnit test results will be generate in this location DeloitteAwayDayPlanner/build/test-results/test

This aplication is working as per specification with 100% covereage 


Thank You Marios