package com.core;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;


import com.google.common.io.Files;

import org.junit.Before;



public class PlannerGeneratorTest {
	
	PlannerGenerator pg;
	
	static private final String start = "09:00 AM";
	static private final String lunch = "12:00 PM";
	static private final String finish = "05:00 PM";
	static private final String random = "10:45 AM";
	private String testDataPath;
	
	@Before
	public void setUp() throws Exception {
		
		pg = new PlannerGenerator();
		testDataPath = "src/test/resources/input_data.txt";	
	}
	
	@Test
	public void testPlannerTime() throws Exception {
		setUp();
		pg.initialiseTime(start);
		assertEquals("09:00 am", pg.getTime());
		
		pg.updateTime(30);
		assertEquals("09:30 am", pg.getTime());
		
		pg.updateTime(-60);
		assertEquals("08:30 am", pg.getTime());
	}
	
	@Test
	public void testAcitivityOverun() throws Exception {
		setUp();
		pg.initialiseTime(start);
		assertFalse(pg.isOverun(lunch, 60));

		pg.initialiseTime(random);
		pg.updateTime(30);
		assertTrue(pg.isOverun(lunch, 60));
	}
	
	@Test
	public void testFetchData() throws Exception {
		setUp();
		
		ConcurrentHashMap<String,String> testData = pg.fetchData(testDataPath);
		
		assertNotEquals(null, testData);
		assertEquals(20, testData.size());
	}
	
	@Test
	public void testGeneratePlanner() throws Exception {
		setUp();
		String expected_out = "src/test/resources/expected_output.txt";	
		
		ConcurrentHashMap<String,String> testData = pg.fetchData(testDataPath);
		
		pg.generatePlanner(testData, PlannerGenerator.MORNING);
		
		assertEquals(Files.asCharSource(new File(expected_out), Charset.forName("UTF-8")).read(),pg.output.toString());
	}
}
