package com.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class used to retrieve given properties in the configuration file
 * @author marios odigie
 *
 */
public class ProjectProperties {
	
    /**
     * Set a constant taken from the config.properties file.
     *
     * @param constant the constant to set.
     * @return Property - value (the value for the constant)
     */
    public String property(String constant) {
        String value = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            String path = System.getProperty("user.dir");
            path=path.replace("src/main/java", "");
            input = new FileInputStream(path+"/config.properties");
            prop.load(input);
            value = prop.getProperty(constant);
        } catch (IOException ex) {
            System.out.println("Connection to Config file failed \n"+ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                	System.out.println("Connection to Config file failed \n"+e);
                }
            }
        }
        return value;
    }

}
