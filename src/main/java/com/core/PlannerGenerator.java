package com.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.config.ProjectProperties;

/**
 * Main Class to schedule an activity planner
 * @author marios odigie
 *
 */

public class PlannerGenerator {
	
	Calendar time;
	SimpleDateFormat formatTime;
	SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
	
	static ProjectProperties properties = new ProjectProperties();

	static private final String start = properties.property("day_start_time"); 
	static private final String lunch = properties.property("day_lunch_time"); 
	static private final String finish = properties.property("day_finish_time"); 
	
	static final String MORNING = "morning_session";
	static final String NOON = "noon_session";
	
	int team = 0;
	StringBuilder output = new StringBuilder();

	public static void main(String[] args) throws IOException, ParseException {
		
		
		//get input data file path
		if (args.length != 0 && new File(args[0]).exists()) {
			
			PlannerGenerator generator = new PlannerGenerator();
			generator.startProcess(args[0]);

			System.out.print(generator.output);
			
		}else {
			System.out.println("Invalid input file");
			System.exit(0);
		}	
	}
	
	protected void startProcess(String path) throws IOException, ParseException{
		
		ConcurrentHashMap<String,String> dataMap = fetchData(path);
		
		generatePlanner(dataMap,MORNING);

	}
	
	/**
	 * Fetch data from the path argument parsed in
	 * @throws ParseException 
	 */	
	public ConcurrentHashMap<String,String>  fetchData(String path) throws IOException, ParseException{
		
		ConcurrentHashMap<String,String> activitiesMap= new ConcurrentHashMap<String, String>();
		
		BufferedReader bf = new BufferedReader(new FileReader(path)); 
		String line = bf.readLine();
		while (line != null) {
			
			/*special regex: it will split the string at spaces that are followed 
			by a 2digit-number+'min'or the word 'sprint'.*/
			String[] splitted = line.split("[ ](?=[0-9]{2}min|sprint)");
			activitiesMap.put(splitted[0], splitted[1]);
			line = bf.readLine();
		}
		return activitiesMap;
		//generatePlanner(activitiesMap,MORNING);

	}
	
	/**
	 * Build the output by querying the data map
	 * @param planMap- containing activities data
	 * @param session- information about the part of the day being scheduled 
	 * @throws ParseException 
	 */	
	protected void generatePlanner(ConcurrentHashMap<String,String> map, String session) throws ParseException {
	
		if(map.isEmpty())
			return;
		
		if(session.equals(MORNING)) {
			initialiseTime(start);
			team++;
    		output.append("Team "+team+ ":");
    		output.append(System.lineSeparator());
		
    		iterateMap(map,lunch);
    		
    		initialiseTime(lunch);
    		output.append("12:00 pm : Lunch Break 60min");
    		output.append(System.lineSeparator());
    		updateTime(60);
    		session = NOON;
		
		}
		else {
			
			iterateMap(map,finish);
			
    		output.append("05:00 pm : Staff Motivation Presentation");
    		output.append(System.lineSeparator());
    		output.append(System.lineSeparator());
    		
    		session = MORNING;
		}	
		generatePlanner(map, session);
	}
	
	/**
	 * Takes in and iterates through a populated HashMap object to build an output
	 * @param map- containing activities data
	 * @param timeLimit- Time limit of the activities that can be added
	 * @throws ParseException 
	 */	
	private ConcurrentHashMap<String,String> iterateMap(ConcurrentHashMap<String,String> map, String timeLimit) throws ParseException {
		
		//Iterate through the activities in the map and print them
		for(Entry<String, String> entry : map.entrySet()) {
			
		    String key = entry.getKey();
		    String value = entry.getValue();
		    int intValue =0;
		    
		    //The trim() method is used in case there is any trailing spaces at the end of the file
		    if(value.trim().equals("sprint"))
		    	intValue= 15;
		    else
		    	 //Gets the number value of activity duration ignoring the letters
		    	 intValue = Integer.parseInt(value.replaceAll("[\\D]", ""));
	    	
		    if(!isOverun(timeLimit, intValue)) {
	    		output.append(getTime()+" : "+key+"  "+value);
	    		output.append(System.lineSeparator());
	    		updateTime(intValue);
	    		map.remove(key, value);
		    }
		}
		return map;
	}	
	
	/**
	 * Initialise the time for the session to commence
	 * @throws ParseException 
	 */	
	protected void initialiseTime(String initTime) throws ParseException{
		
		time = Calendar.getInstance();
		Date start = dateFormat.parse(initTime);
		time.setTime(start);
		formatTime = dateFormat;

	}

	/**
	 * Gets instance of the current time
	 */	
	public String getTime(){
		return formatTime.format(time.getTime()).toLowerCase();
	}
	
	/**
	 * Updates the the current time
	 * @param timeUpdate- Adding the duration of the activity to the current time.
	 */	
	public void updateTime(int timeUpdate){
		time.add(Calendar.MINUTE, timeUpdate);
		
	}
	/**
	 * Checks if an activity will overrun
	 * @param timeUpdate- Adding the duration of the activity to the current time.
	 * @throws ParseException 
	 */	
	public boolean isOverun(String limit, int duration ) throws ParseException{
		Calendar timeLimit = Calendar.getInstance();
		Date start = dateFormat.parse(limit);
		timeLimit.setTime(start);
		
		updateTime(duration);
		boolean status = time.after(timeLimit);
		updateTime(-duration);
		
		return status;
	}
}
